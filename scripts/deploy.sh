#!/bin/sh

set -e
set -x

oc create -f chart/crds/*.yaml
curl https://gitlab.cern.ch/paas-tools/operators/authz-operator/-/raw/master/config/crd/bases/webservices.cern.ch_oidcreturnuris.yaml | oc create -f -
curl https://gitlab.cern.ch/paas-tools/operators/authz-operator/-/raw/master/config/crd/bases/webservices.cern.ch_applicationregistrations.yaml | oc create -f -
