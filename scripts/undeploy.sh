#!/bin/sh

set -e
set -x

oc delete webeossites --all
oc delete -f chart/crds/*.yaml
oc delete project webeos
