package controllers

import (
	"bytes"
	"os"
	"path/filepath"
	"text/template"

	"github.com/Masterminds/sprig"
	webeosSitev1alpha1 "gitlab.cern.ch/webservices/webeos/webeos-site-operator/api/v1alpha1"
)

// ApachePath contains the Path to the Apache folder eg."/etc/httpd/sites-available"
var ApachePath string

// VhostTemplate contains the Template of vhost
var VhostTemplate *template.Template

func createVhostConfig(class SiteDefinition) (string, error) {
	var err error
	var tpl bytes.Buffer

	if err = VhostTemplate.Execute(&tpl, class); err != nil {
		return "", err
	}

	vhostData := tpl.String()

	return vhostData, nil
}

func writeConfigToFile(webeosSiteInstance webeosSitev1alpha1.WebeosSite, content string) error {
	filename := getSiteFileConfName(webeosSiteInstance)
	// Only the Apache master process should have access to the config files.
	// Use mode 600 to avoid leaking config files to worker processes running as unprivileged user.
	f, err := os.OpenFile(getVhostFilePath(filename), os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	_, err = f.WriteString(content)
	if err != nil {
		f.Close()
		return err
	}
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}

func removeVhostConfig(webeosSiteInstance webeosSitev1alpha1.WebeosSite) {
	filename := getSiteFileConfName(webeosSiteInstance)
	//Errors returned from Remove are ingnored in order not to reconcile again.
	os.Remove(getVhostFilePath(filename))
}

func getVhostFilePath(siteName string) string {
	return filepath.Join(ApachePath, siteName+".conf")
}

// LoadTemplate loads an Apache Config Template
func LoadTemplate(templatePath string) {
	vhostTemplateFileName := filepath.Base(templatePath)
	// Absolute path given in the comand
	VhostTemplate = template.Must(template.New(vhostTemplateFileName).Funcs(sprig.TxtFuncMap()).ParseFiles(templatePath))
}
