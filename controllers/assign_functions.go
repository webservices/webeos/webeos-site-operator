package controllers

import (
	"context"
	"errors"
	"fmt"
	"math/rand"
	"regexp"
	"time"

	"github.com/go-logr/logr"

	webeosv1alpha1 "gitlab.cern.ch/webservices/webeos/webeos-site-operator/api/v1alpha1"
	utils "gitlab.cern.ch/webservices/webeos/webeos-site-operator/utils"
	corev1 "k8s.io/api/core/v1"
	goCli "sigs.k8s.io/controller-runtime/pkg/client"
)

type availableDeployment struct {
	Name        string
	Annotations map[string]string
}

const (
	webEosSiteRegexAnnotation         = "webeos.cern.ch/webeos-site-path-regex"
	planeNewWebEosSiteLabel           = "webeos.cern.ch/place-new-webeos-sites"
	webeosSiteServerVersionAnnotation = "webeos.cern.ch/webeos-site-server-version"
)

func assignRouterShard(instance *webeosv1alpha1.WebeosSite, log logr.Logger) error {
	if instance.Spec.ServerDetails.AssignedRouterShard != "" {
		return nil
	}

	if shards == nil {
		return errors.New("specify a valid list of available router shards with the --assignable-router-shard flag")
	}

	randomShard := utils.RandomStrElement(shards)
	instance.Spec.ServerDetails.AssignedRouterShard = randomShard
	log.Info(fmt.Sprintf("Assigned router shard %v to a webeos site", instance.Spec.ServerDetails.AssignedRouterShard))

	return nil
}

func (r *WebeosSiteReconciler) getAllWebeosDeployments() ([]availableDeployment, error) {
	labels := map[string]string{planeNewWebEosSiteLabel: "true"}
	serviceList := &corev1.ServiceList{}

	if err := r.List(context.TODO(), serviceList, goCli.MatchingLabels(labels)); err != nil {
		return nil, err
	}

	ret := []availableDeployment{}
	for _, service := range serviceList.Items {
		ret = append(ret, availableDeployment{Name: service.Name, Annotations: service.Annotations})
	}

	return ret, nil
}

// Logic behind this function
//  1. Find the list of deployments with label webeos.cern.ch/place-new-webeos-sites=true
//  2. For each deployment check:
//     -  if the new site's EOS path matches the webeos.cern.ch/webeos-site-path-prefix annotation,
//     assign the site to the webeos deployment with the best match (longest prefix that matches the site's path).
//     -  the new site's Server Version and assign it to the deplpoyment that matches the webeos.cern.ch/webeos-site-server-version annotation.
//  3. If several deployments are found with the same prefix length, pick one at random.
func matchBestDeployment(deployments []availableDeployment, instance *webeosv1alpha1.WebeosSite) (availableDeployment, error) {
	instanceEosPath := instance.Spec.SitePath
	instanceServerVersion := instance.Spec.ServerVersion
	eligibleDeployments := []availableDeployment{}

	for _, deployment := range deployments {
		if isDeploymentEligibleForAssignment(deployment, instanceEosPath, instanceServerVersion) {
			eligibleDeployments = append(eligibleDeployments, deployment)
		}
	}

	if len(eligibleDeployments) == 0 {
		return availableDeployment{}, errors.New("No eligible deployments found")
	}

	bestScore := 0
	scores := make([]int, len(eligibleDeployments))

	for deployIndex, deployment := range eligibleDeployments {
		pathRegex := deployment.Annotations[webEosSiteRegexAnnotation]
		re, err := regexp.Compile(pathRegex)
		if err != nil {
			return availableDeployment{}, errors.New("Invalid sitePath regex")
		}

		score := len(re.FindString(instanceEosPath))
		scores[deployIndex] = score
		if score > bestScore {
			bestScore = score
		}
	}

	highestScoreDeployments := []availableDeployment{}
	for index, deployment := range eligibleDeployments {
		if scores[index] == bestScore {
			highestScoreDeployments = append(highestScoreDeployments, deployment)
		}
	}

	if len(highestScoreDeployments) == 1 {
		return highestScoreDeployments[0], nil
	}

	s := rand.NewSource(time.Now().Unix())
	r := rand.New(s)
	return highestScoreDeployments[r.Intn(len(highestScoreDeployments))], nil
}

func isDeploymentEligibleForAssignment(deployment availableDeployment, eosPath string, serverVersion webeosv1alpha1.ServerVersionType) bool {
	eosSitePathRegex := deployment.Annotations[webEosSiteRegexAnnotation]
	eosSiteServerVersion := deployment.Annotations[webeosSiteServerVersionAnnotation]
	re, err := regexp.Compile(eosSitePathRegex)
	if err != nil {
		return false
	}
	return re.MatchString(eosPath) && (eosSiteServerVersion == string(serverVersion))
}

func (r *WebeosSiteReconciler) assignWebEOSDeployment(instance *webeosv1alpha1.WebeosSite) error {
	if instance.Spec.ServerDetails.AssignedDeploymentShard != "" {
		return nil
	}

	availableDeployments, err := r.getAllWebeosDeployments()
	if err != nil {
		return err
	} else if len(availableDeployments) == 0 {
		return errors.New("There is no available deployments at the moment")
	}

	bestDeployment, err := matchBestDeployment(availableDeployments, instance)
	if err != nil {
		return err
	}
	// Check if availableDeployment.Name is longer than 63 characters as this value is
	// stored in Kubernetes labels and there is limited length as mentioned
	// in https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set
	if len(bestDeployment.Name) > 63 {
		return errors.New("Available webeos deployment is longer than 63 and it can not be stored in a label")
	}

	instance.Spec.ServerDetails.AssignedDeploymentShard = bestDeployment.Name
	r.Log.Info(fmt.Sprintf("Assigned webeos deployment %v to webeos site", instance.Spec.ServerDetails.AssignedDeploymentShard))

	return nil
}
