package controllers

import (
	"context"
	"fmt"
	"os"

	webeosSitev1alpha1 "gitlab.cern.ch/webservices/webeos/webeos-site-operator/api/v1alpha1"

	config "github.com/openshift/api/config/v1"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/types"

	"github.com/go-logr/logr"

	"k8s.io/apimachinery/pkg/api/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
)

// LabelSelector is populated by cmdline argument and specifies in which deployment the operator is running in
var LabelSelector *v1.LabelSelector

// ReadinessProbePath is populated by the cmdline argument and specifies the path of the file to touch when first loop is completed
var ReadinessProbePath string

// NetworkList of clusternetworks.network.openshift.io/default
var NetworkList []string

var log = logf.Log.WithName("controller_webeosconfig")

// ReconcileWebeosSite reconciles a WebeosSite object
type ReconcileWebeosSite struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=webeos.webservices.cern.ch,resources=webeossites,verbs=get;list;watch
// +kubebuilder:rbac:groups=network.openshift.io,resources=clusternetworks,verbs=get;list;watch

// Reconcile reads that state of the cluster for a WebeosSite object and makes changes based on the state read
// and what is in the WebeosSite.Spec
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileWebeosSite) Reconcile(ctx context.Context, request ctrl.Request) (ctrl.Result, error) {
	_ = r.Log.WithValues("webeossite", request.NamespacedName)
	r.Log.Info("Reconciling WebeosSite")

	// Fetch the WebeosSite instance
	webeosSiteInstance := &webeosSitev1alpha1.WebeosSite{}
	err := r.Get(ctx, request.NamespacedName, webeosSiteInstance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected.
			r.Log.Error(err, fmt.Sprintf("webeosSiteInstance '%v' not found", webeosSiteInstance.Name))
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		r.Log.Error(err, "error reading the object, requeing the request")
		return ctrl.Result{}, err
	}

	err = processWebEosSiteInstance(*webeosSiteInstance)
	if err != nil {
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, nil
}

// SetupWithManager setup with manager
func (r *ReconcileWebeosSite) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&webeosSitev1alpha1.WebeosSite{}).
		Complete(r)
}

// In the case of an empty searchedLabels, we return True as it means it will operate every CR
func matchesLabel(instance map[string]string, searchedLabels map[string]string) bool {
	for key, val := range searchedLabels {
		if instance[key] != val {
			return false
		}
	}
	return true

}

// PrepareReadinessProbe is a function used on the main to make a first iteration through all the CRs
func PrepareReadinessProbe(namespace string) error {
	var err error
	var config *rest.Config
	var readinessLog = logf.Log.WithName("Preparing Probe")

	if os.Getenv("KUBECONFIG") == "" {
		readinessLog.Info("using in-cluster configuration")
		config, err = rest.InClusterConfig()
	} else {
		readinessLog.Info(fmt.Sprintf("using configuration from '%s'", os.Getenv("KUBECONFIG")))
		config, err = clientcmd.BuildConfigFromFlags("", os.Getenv("KUBECONFIG"))
	}
	if err != nil {
		return err
	}

	// Registering in the config the schema od the CRD
	webeosSitev1alpha1.SchemeBuilder.AddToScheme(scheme.Scheme)

	// Setting up the group and version for client config
	config.ContentConfig.GroupVersion = &webeosSitev1alpha1.GroupVersion
	config.APIPath = "/apis"
	config.NegotiatedSerializer = scheme.Codecs.WithoutConversion()
	config.UserAgent = rest.DefaultKubernetesUserAgent()

	// Creates the clientset
	clientset, err := rest.UnversionedRESTClientFor(config)
	if err != nil {
		return err
	}

	options := v1.ListOptions{
		LabelSelector: labels.SelectorFromSet(LabelSelector.MatchLabels).String(),
	}

	result := webeosSitev1alpha1.WebeosSiteList{}

	err = clientset.
		Get().
		Namespace(namespace).
		Resource("webeossites").
		VersionedParams(&options, scheme.ParameterCodec).
		Do(context.TODO()).
		Into(&result)

	if err != nil {
		return err
	}

	for _, webeosSiteInstance := range result.Items {
		processWebEosSiteInstance(webeosSiteInstance)
	}
	return touch(ReadinessProbePath)
}

func processWebEosSiteInstance(webeosSiteInstance webeosSitev1alpha1.WebeosSite) error {
	reqLogger := log.WithName("Processing WebEosSite Instance")
	// Check if the webeosSiteInstance was marked to be deleted
	if webeosSiteInstance.GetDeletionTimestamp() != nil {
		removeVhostConfig(webeosSiteInstance)
		reqLogger.Info(fmt.Sprintf("webeosSiteInstance '%v' deleted", webeosSiteInstance.Name))
		return nil
	}

	// Make sure the site is in the scope of this deployment
	if !matchesLabel(webeosSiteInstance.GetObjectMeta().GetLabels(), LabelSelector.MatchLabels) {
		removeVhostConfig(webeosSiteInstance)
		reqLogger.Info(fmt.Sprintf("webeosSiteInstance '%v' is outside of the scope of this controller", webeosSiteInstance.Name))
		return nil
	}

	// Check if any of the required fields on the webeosSite CR are empty
	// If a field is missing, remove the site configuration
	siteDefinition, errList := createSiteDefinitionFromCR(reqLogger, webeosSiteInstance.Spec)
	if len(errList) > 0 {
		removeVhostConfig(webeosSiteInstance)
		for _, err := range errList {
			reqLogger.Error(err, fmt.Sprintf("Skipping invalid webeosSite %v", webeosSiteInstance.Name))
		}
		return nil
	}

	// we eliminated all cases where we did NOT want to create the site configuration,
	// so we can now create it
	vhostData, err := createVhostConfig(siteDefinition)
	if err != nil {
		reqLogger.Error(err, fmt.Sprintf("Error creating vhostConfig template for '%v", webeosSiteInstance.Name))
		// we could not generate a valid vhost for this webeosSite CR. No use retrying
		// until the webeosSite CR changes, so return reconcile success.
		return nil
	}

	// Path to vhost file
	if err := writeConfigToFile(webeosSiteInstance, vhostData); err != nil {
		reqLogger.Error(err, fmt.Sprintf("Error writing vhostConfig for '%v", webeosSiteInstance.Name))
		// We could not write the vhost config file for this webeosSite CR.
		// Could be due to temporary glitch, fail reconcile loop so the operation is retried.
		return err
	}

	reqLogger.Info(fmt.Sprintf("'%v' Vhost configuration created successfully", webeosSiteInstance.Name))
	return nil
}

// getSiteFileConfName returns the Filename for a given WebeosSite
func getSiteFileConfName(webeosSiteInstance webeosSitev1alpha1.WebeosSite) string {
	// Set .conf filename to be FQDN.conf
	// so we cannot have a new vhost file using the same FQDN as an old vhost file
	// for an old site that has been deleted but not cleaned up
	return webeosSiteInstance.Spec.HostName
}

// GetPodNetwork Gets IP addresses from OKD4 Pod Network to be added to Apache Configuration
func GetPodNetwork(client client.Client) error {
	network := &config.Network{}
	name := types.NamespacedName{
		Name: "cluster",
	}
	err := client.Get(context.TODO(), name, network)
	if err != nil {
		return err
	}
	for _, entry := range network.Status.ClusterNetwork {
		NetworkList = append(NetworkList, entry.CIDR)
	}
	return nil
}

func touch(path string) error {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		file, err := os.Create(path)
		if err != nil {
			return err
		}
		defer file.Close()
	} else {
		f, err := os.OpenFile(path, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0600)
		if err != nil {
			f.Close()
			return err
		}
		err = f.Close()
		if err != nil {
			return err
		}
	}
	return nil
}
