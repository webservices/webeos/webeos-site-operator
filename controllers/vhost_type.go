package controllers

// SiteDefinition defines the neccessary settings for vhost configuration
// ClientID and ClientSecret are the values required to configure the OIDC
// CryptoPassphrase is an OIDC element needed for session/state encryption purposes
// AllowAnonAccessByDefault is the flag used for legacy websites where the default case is to be accessed by everyone
// Archived is the flag for archived websites
// CGIPath is the path for the common Gateway interface
// LegacyBaseURL contains the value that will be added to the site URL according to UseLegacyBaseURL
// UseLegacyBaseURL is the flag for legacy sites
// AllowedWebCrawlersIfNoRobotsTxt specifies what robots.txt should be served to the browser
// OIDCCachePath specifies the OIDC Cache folder
type SiteDefinition struct {
	ClientID, ClientSecret, CryptoPassphrase, RedirectURI, FQDN, ShortSitename, DocumentRoot, LegacyBaseURL, IssuerURL, RedirectLocation, AllowedWebCrawlersIfNoRobotsTxt, OIDCCachePath string
	AllowAnonAccessByDefault, Archived, UseLegacyBaseURL, UseDistributedConfigurationFiles                                                                                               bool
	CgiPaths, ClusterNetworks                                                                                                                                                            []string
}
