/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	generalerrors "errors"
	"flag"
	"fmt"
	"net/url"
	"strconv"
	"time"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	v1 "k8s.io/api/core/v1"
	meta "k8s.io/apimachinery/pkg/api/meta"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	"k8s.io/apimachinery/pkg/api/errors"

	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/authz-operator/api/v1alpha1"
	webeosv1alpha1 "gitlab.cern.ch/webservices/webeos/webeos-site-operator/api/v1alpha1"
)

const (
	updFinalizer            = "webeossitemgmt.webservices.cern.ch"
	projectBlockedLabel     = "okd.cern.ch/project-blocked"
	blockedWebEOSDeployment = "invalid-deployment-for-blocked-sites"
)

var (
	oidcReturnPath       = flag.String("oidc-return-path", "/oidc/callback", "Trailing portion of the OidcReturnURI")
	webeossitesNamespace = flag.String("webeos-sites-namespace", "webeos", "Namespace where the WebeosSites owned by UserProvidedDirectories CRs will be created ")
)

// UserProvidedDirectoryReconciler reconciles a UserProvidedDirectory object
type UserProvidedDirectoryReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=webeos.webservices.cern.ch,resources=userprovideddirectories,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=webeos.webservices.cern.ch,resources=userprovideddirectories/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=core,resources=namespaces,verbs=get;list;watch

// Reconcile reconcile UserProvidedDirectory resource
func (r *UserProvidedDirectoryReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = r.Log.WithValues("userprovideddirectory", req.NamespacedName)

	result := ctrl.Result{}

	r.Log.Info("Reconciling request")

	// Fetch the UserProvidedDirectory instance
	upd := &webeosv1alpha1.UserProvidedDirectory{}
	if err := r.Get(ctx, req.NamespacedName, upd); err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup
			// logic we use finalizers. Return and don't requeue
			r.Log.Info("UserProvidedDirectory not found; return and don't requeue")
			return result, nil
		}
		return result, err
	}

	// Check if the CR was marked to be deleted
	if upd.GetDeletionTimestamp() != nil {
		// If marked for deletion we will want to check if it still has our finalizer
		// if indeed it has it, we want to execute some finalizer logic and remove the finalizer
		// otherwise we just return
		if controllerutil.ContainsFinalizer(upd, updFinalizer) {
			return result, r.ensureFinalizerLogic(ctx, upd)
		}
		return result, nil
	}

	// Check if finalizer is set, if not, set it and update the UPD, return after that
	if !controllerutil.ContainsFinalizer(upd, updFinalizer) {
		r.Log.Info("Adding finalizer to UserProvidedDirectory")
		controllerutil.AddFinalizer(upd, updFinalizer)
		if err := r.Update(ctx, upd); err != nil {
			r.Log.Error(err, "Failed to update UserProvidedDirectory when adding the finalizer")
			return result, err
		}
		return result, nil
	}

	// If it is not set, initialize the status of the CR and requeue the reconciliation
	if meta.FindStatusCondition(upd.Status.Conditions, webeosv1alpha1.UpdReady) == nil {
		if err := r.setInitializingStatus(ctx, upd); err != nil {
			return result, err
		}
		return result, nil
	}

	// Retrieve ApplicationRegistration needed to provision a WebeosSite
	appReg, err := r.getApplicationRegistration(ctx, upd)
	if err != nil {
		return result, err
	}

	// Ensure we have a valid ApplicationRegistration. This can happen when the ApplicationRegistration is `Created` (the website should be up and running)
	// or when the ApplicationRegistration has state `DeletedFromAPI` (the website should be blocked). To check if the ApplicationRegistration is in one of those two states,
	// we can check if the oidc secret exists.
	oidcSecret, err := r.getOidcSecret(ctx, upd, appReg)
	if err != nil {
		if err := r.updateStatusWithMissingAppReg(upd); err != nil {
			r.Log.Error(err, "Failed to update CR with a status with missing ApplicationRegistration")
			return result, err
		}
		// Since sometimes it takes a bit for the Authz-Operator to provision an Application registation let's simply
		// requeue after 5 seconds so we don't spam the logs and do not re-try too much
		r.Log.Info(fmt.Sprintf("Waiting for ApplicationRegistration %v to generate a secret containing OIDC client credentials", applicationRegistrationNamespacedName(*upd).Name))
		return ctrl.Result{RequeueAfter: time.Second * 5}, nil
	}

	// Get namespace needed to manage some WebeosSite configuration with namespace annotations
	namespace := &corev1.Namespace{}
	namespaceName := types.NamespacedName{
		Namespace: upd.Namespace,
		Name:      upd.Namespace,
	}
	if err := r.Client.Get(ctx, namespaceName, namespace); err != nil {
		return ctrl.Result{}, err
	}

	// Ensure that a WebeosSite is either created or updated
	if err := r.ensureWebeosSite(*upd, *appReg, *oidcSecret, *namespace); err != nil {
		r.Log.Error(err, "Error creating/updating WebeosSite")
		return result, err
	}

	// Ensure that a OidcReturnURI is either created or updated
	if err := r.ensureOidcReturnURI(*upd); err != nil {
		r.Log.Error(err, "Error creating/updating OidcReturnURI")
		return result, err
	}

	//Update the Status of the UserProvidedDirectory
	meta.SetStatusCondition(&upd.Status.Conditions, metav1.Condition{
		Type:    webeosv1alpha1.UpdReady,
		Status:  metav1.ConditionTrue,
		Reason:  "Successful",
		Message: "Awaiting next reconciliation",
	})
	if err := r.Status().Update(ctx, upd); err != nil {
		r.Log.Error(err, "Failed to update UserProvidedDirectory Status")
		return result, err
	}

	return result, nil
}

func (r *UserProvidedDirectoryReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&webeosv1alpha1.UserProvidedDirectory{}).
		Owns(&webservicesv1alpha1.OidcReturnURI{}).
		// We want to reconcile the UPDs in a namespace whenever there is a change in it
		// because some webeossite configuration can be set via namespace annotations.
		Watches(&corev1.Namespace{}, handler.EnqueueRequestsFromMapFunc(
			func(ctx context.Context, ns client.Object) []reconcile.Request {
				log := r.Log.WithValues("Source", "Namespace event handler", "Namespace", ns.GetName())
				// Fetch UPDs in the same namespace
				upds := &webeosv1alpha1.UserProvidedDirectoryList{}
				options := client.ListOptions{
					Namespace: ns.GetName(),
				}
				err := mgr.GetClient().List(ctx, upds, &options)
				if err != nil {
					log.Error(err, "Couldn't fetch upds in the namespace")
					return []reconcile.Request{}
				}
				requests := make([]reconcile.Request, len(upds.Items))
				for i, upd := range upds.Items {
					requests[i].Name = upd.Name
					requests[i].Namespace = upd.Namespace
					log.V(5).Info("Watching namespace", "upd", upd.Name)
				}
				return requests
			}),
		).
		Complete(r)
}

// ensureFinalizerLogic function responsible for taking the necessary steps related with
// deletion of a UserProvidedDirectory
func (r *UserProvidedDirectoryReconciler) ensureFinalizerLogic(ctx context.Context, upd *webeosv1alpha1.UserProvidedDirectory) error {
	// Since we create WebeosSites in a different namespace (webeos) than the one where the UPD
	// was created, we have to explicitly delete them, as K8S garbage collection will not do it for us
	// if we were to simply set the OwnerReference when we create a WebeosSites
	webeosSite := &webeosv1alpha1.WebeosSite{}
	err := r.Get(ctx, webeosSiteNamespacedName(*upd), webeosSite)
	// If we get an error and it's not a NotFound we will want to print the error since it's not
	// allowing us to delete the WebeosSite
	if err != nil && !errors.IsNotFound(err) {
		r.Log.Error(err, "Failed to get Webeos site, in finalizer routine")
		return err
	}
	// If we didn't get any error and the webeosSite is still not marked for deletion then we
	// want to delete it
	if err == nil && webeosSite.GetDeletionTimestamp() == nil {
		if err := r.Delete(ctx, webeosSite); err != nil {
			r.Log.Error(err, "Failed to delete Webeos site, in finalizer routine")
			return err
		}
	}
	// Finally we want to remove the finalizer from the UPD, we don't need to worry about
	// OidcReturnURI since it lives in the same namespace as the UPD and it will be GC by K8S
	// since when we create it we all add an owner reference
	controllerutil.RemoveFinalizer(upd, updFinalizer)
	if err := r.Update(ctx, upd); err != nil {
		r.Log.Error(err, "Failed to update UserProvidedDirectory when removing the finalizer")
		return err
	}
	return nil
}

// setInitializingStatus sets status to initializing, letting the user know that we are working on reconciling it
func (r *UserProvidedDirectoryReconciler) setInitializingStatus(ctx context.Context, upd *webeosv1alpha1.UserProvidedDirectory) error {
	r.Log.Info("Initializing status of UserProvidedDirectory")
	upd.Status = webeosv1alpha1.UserProvidedDirectoryStatus{}
	meta.SetStatusCondition(&upd.Status.Conditions, metav1.Condition{
		// TODO review how should the UPD be initialized
		Type:    webeosv1alpha1.UpdReady,
		Status:  metav1.ConditionUnknown,
		Reason:  "Initializing",
		Message: "CR initializing",
	},
	)
	if err := r.Status().Update(ctx, upd); err != nil {
		r.Log.Error(err, "Failed to initialize CR with a status")
		return err
	}
	return nil
}

// getApplicationRegistration function that will try to get an ApplicationRegistration and return it if it there is an
// error while trying to get the ApplicationRegistration it will update the UserProvidedDirectory accordingly
func (r *UserProvidedDirectoryReconciler) getApplicationRegistration(ctx context.Context, upd *webeosv1alpha1.UserProvidedDirectory) (*webservicesv1alpha1.ApplicationRegistration, error) {
	appReg := &webservicesv1alpha1.ApplicationRegistration{}
	if err := r.Get(ctx, applicationRegistrationNamespacedName(*upd), appReg); err != nil {
		// If we got an error and it's a NotFound we will update the UPD status
		// to reflect this and then we will requeue
		if errors.IsNotFound(err) {
			if err := r.updateStatusWithMissingAppReg(upd); err != nil {
				r.Log.Error(err, "Failed to update CR with a status with missing ApplicationRegistration")
				return nil, err
			}
			// Since we create ApplicationRegistrations when we create a namespace for the user the ApplicationRegistration not being present it's
			// a persistent error, so print the error and requeue
			return nil, fmt.Errorf("ApplicationRegistration %v was not found, either it wasn't created or it was deleted", applicationRegistrationNamespacedName(*upd).Name)
		} else {
			r.Log.Error(err, "Failed to fetch ApplicationRegistration")
			return nil, err
		}
	}
	return appReg, nil
}

// ensureWebeosSite this function will either create or update resource the WebeosSite based on the
// resources passed as parameters
func (r *UserProvidedDirectoryReconciler) ensureWebeosSite(upd webeosv1alpha1.UserProvidedDirectory, appReg webservicesv1alpha1.ApplicationRegistration, oidcSecret v1.Secret, namespace corev1.Namespace) error {
	var webeosSite webeosv1alpha1.WebeosSite
	// We have to do this part here as MutateFn cannot mutate object
	// name and/or object namespace
	webeosSite.Name = webeosSiteNamespacedName(upd).Name
	webeosSite.Namespace = webeosSiteNamespacedName(upd).Namespace

	// This function is an auxiliary function from controller runtime that it will either
	// create or update an object but before doing it, it will always run MutateFn, the function specified below
	_, err := controllerutil.CreateOrUpdate(context.TODO(), r.Client, &webeosSite, func() error {
		// Call to the function that will update the instance webeosSite (the only variable passed by referance) with the updated information
		// from the three variable upd, appReg and oidcSecret
		modifyWebeosSite(upd, appReg, oidcSecret, &webeosSite, namespace)
		// Here we would tipically return with the SetOwnerReference function, however since this resource is created on
		// another namespace K8S would simply generate an error as cross namespace ownership is not supported
		return nil
	})
	return err
}

// modifyWebeosSite function that will perform all the necessary logic so that in the end
// the parameter webeosSite will be updated accordingly
func modifyWebeosSite(upd webeosv1alpha1.UserProvidedDirectory, appReg webservicesv1alpha1.ApplicationRegistration, oidcSecret v1.Secret, webeosSite *webeosv1alpha1.WebeosSite, namespace corev1.Namespace) {
	if webeosSite.Annotations == nil {
		webeosSite.Annotations = make(map[string]string)
	}

	// Set up ownership annotations simply to allow us to easily
	// know to which UPD a WebeosSite corresponds to
	webeosSite.Annotations["operator-sdk/primary-resource"] = upd.Namespace + "/" + upd.Name
	webeosSite.Annotations["operator-sdk/primary-resource-type"] = "UserProvidedDirectory.webeos.webservices.cern.ch"

	if webeosSite.Labels == nil {
		webeosSite.Labels = make(map[string]string)
	}

	webeosSite.Spec.SiteDescription = fmt.Sprintf("Created by webeossitemgmt operator for UserProvidedDirectory %s in namespace %s", upd.Name, upd.Namespace)
	webeosSite.Spec.Type = "ownerProvidedEosFolder"
	// SitePath: No need to do validation as this will happen at the creation of the CR and the
	// webeos-site operator will also preform it
	webeosSite.Spec.SitePath = upd.Spec.EosPath
	// In https://codimd.web.cern.ch/prA1f5CHT2eaOYef33uljw?view it was decided that the UPD CRs would
	// start to be created with the full hostname as their name, so we can do this attribution here.
	webeosSite.Spec.HostName = getFQDN(upd)
	// the ApplicationRegistration resource in the CR's namespace gives us the owner.
	webeosSite.Spec.Owner = appReg.Status.CurrentOwnerUsername
	webeosSite.Spec.Oidc = &webeosv1alpha1.Oidc{
		ClientID:     string(oidcSecret.Data["clientID"]),
		ClientSecret: string(oidcSecret.Data["clientSecret"]),
		IssuerURL:    string(oidcSecret.Data["issuerURL"]),
		ReturnURI:    getOidcReturnUri(upd),
	}
	webeosSite.Spec.AllowedWebCrawlersIfNoRobotsTxt = upd.Spec.AllowedWebCrawlersIfNoRobotsTxt

	// use defaults (all booleans to false as per https://go.dev/tour/basics/12) if UPD doesn't specify a spec.globalSettings
	if upd.Spec.GlobalSettings == nil {
		upd.Spec.GlobalSettings = &webeosv1alpha1.GlobalSettings{}
	}
	webeosSite.Spec.UseDistributedConfigurationFiles = upd.Spec.GlobalSettings.UseDistributedConfigurationFiles
	webeosSite.Spec.AllowAnonAccessByDefault = upd.Spec.GlobalSettings.AllowAnonymousAccessByDefault
	webeosSite.Spec.Archived = upd.Spec.GlobalSettings.ArchiveSite

	// Pass the folderConfiguration structure as-is, since we use the same Type as the WebeosSite CRD
	webeosSite.Spec.Configuration = upd.Spec.FolderConfiguration

	// Setup LegacyBaseURL structure of WebeosSite CR if, the UPD CR has the adequate annotations for it.
	// This structure is needed when migrating websites from the old infra into the new one,
	// otherwise we can omit this structure.
	// preserveLegacyURL is by default true so we don't break existing websites as noted in
	// https://gitlab.cern.ch/paas-tools/operators/webeossitemgmt-operator/-/issues/1#note_3518026
	if value, found := upd.ObjectMeta.Annotations["webservices.cern.ch/webeos-legacybaseurl"]; found {
		var preserveLegacyUrls = true
		preserveLegacyUrls, _ = strconv.ParseBool(upd.ObjectMeta.Annotations["webservices.cern.ch/webeos-uselegacybaseurl"])
		webeosSite.Spec.LegacyBaseURL = &webeosv1alpha1.LegacyBaseURL{
			Path:               value,
			PreserveLegacyUrls: preserveLegacyUrls,
		}
	} else {
		webeosSite.Spec.LegacyBaseURL = nil
	}

	// protect against null-pointer dereferencing when filling in spec.serverDetails
	if webeosSite.Spec.ServerDetails == nil {
		webeosSite.Spec.ServerDetails = &webeosv1alpha1.ServerDetails{}
	}

	if webeosSite.Spec.ServerVersion != upd.Spec.ServerVersion {
		webeosSite.Spec.ServerVersion = upd.Spec.ServerVersion
		// Let the WebEOS Site controller choose a deployment shard automatically
		webeosSite.Spec.ServerDetails.AssignedDeploymentShard = ""
	}

	// Check namespaces's annotations for configuring WebeoSite CR
	if projectBlocked(namespace) {
		// If the project is blocked, point this WebEOS site to an invalid deployment
		// This causes HAProxy routers to return a "503 Application not available" page
		webeosSite.Spec.ServerDetails.AssignedDeploymentShard = blockedWebEOSDeployment
	} else if value, found := namespace.ObjectMeta.Annotations["webeos.webservices.cern.ch/force-webeos-deployment"]; found {
		// If there is an override value on the namespace, use that value as the deployment shard
		webeosSite.Spec.ServerDetails.AssignedDeploymentShard = value
	} else if webeosSite.Spec.ServerDetails.AssignedDeploymentShard == blockedWebEOSDeployment {
		// If the project was blocked (and we are unblocking it now), let the WebEOS Site controller choose a shard automatically
		webeosSite.Spec.ServerDetails.AssignedDeploymentShard = ""
	} else {
		// Leave the assigned deployment shard as-is. We should not reset the deployment shard assigned by the webeossite controller with each reconciliation.
	}
	if value, found := namespace.ObjectMeta.Annotations["webeos.webservices.cern.ch/force-router-shard"]; found {
		webeosSite.Spec.ServerDetails.AssignedRouterShard = value
	} else {
		// If absence of the namespace annotation, keep the previously assigned router shard. We should not reset the router shard assigned by the webeossite controller with each reconciliation.
		// NB: this means that if the annotation was set but then removed, we don't revert the router shard to what it was before the namespace annotation was set.
		// Reverting is not feasible since we have not recorded the previous value.
	}
	if value, found := namespace.ObjectMeta.Annotations["webeos.webservices.cern.ch/force-ip-whitelist"]; found {
		webeosSite.Spec.ServerDetails.RouteIpWhitelist = value
	} else {
		// in absence of this namespace annotation (in particular after it is removed), reset IP whitelist to default
		webeosSite.Spec.ServerDetails.RouteIpWhitelist = ""
	}
}

// ensureOidcReturnURI this function will either create or update resource the OidcReturnURI based on the
// resources passed as parameters
func (r *UserProvidedDirectoryReconciler) ensureOidcReturnURI(upd webeosv1alpha1.UserProvidedDirectory) error {
	var oidcReturnURI webservicesv1alpha1.OidcReturnURI
	// We have to do this part here as MutateFn cannot mutate object
	// name and/or object namespace
	oidcReturnURI.Name = upd.Name
	oidcReturnURI.Namespace = upd.Namespace

	_, err := controllerutil.CreateOrUpdate(context.TODO(), r.Client, &oidcReturnURI, func() error {
		//Since we only want to preform one change in this CR no need to have a seperate function for this
		oidcReturnURI.Spec.RedirectURI = getOidcReturnUri(upd)
		// Set UserProvidedDirectory upd as the owner and controller. This will set
		// the OwnerReferences to the oidcReturnURI, and will trigger a deletion of the
		// oidcReturnURI when the UserProvidedDirectory parent gets deleted.
		return controllerutil.SetOwnerReference(&upd, &oidcReturnURI, r.Scheme)
	})
	return err
}

// updateStatusWithMissingAppReg function to update consistently the Status of a UPD when
// the ApplicationRegistration is missing
func (r *UserProvidedDirectoryReconciler) updateStatusWithMissingAppReg(upd *webeosv1alpha1.UserProvidedDirectory) error {
	meta.SetStatusCondition(&upd.Status.Conditions, metav1.Condition{
		Type:    webeosv1alpha1.UpdReady,
		Status:  metav1.ConditionFalse,
		Reason:  "MissingResource",
		Message: "Waiting for site registration in Application Portal",
	})
	if err := r.Status().Update(context.TODO(), upd); err != nil {
		return err
	}
	return nil
}

// webeosSiteNamespacedName function that given an upd will return the NamespacedName of a WebeosSite which
// allow us to only having to change this function if we want to change the way we call WebeosSites
func webeosSiteNamespacedName(upd webeosv1alpha1.UserProvidedDirectory) types.NamespacedName {
	return types.NamespacedName{
		Namespace: *webeossitesNamespace,
		// All user projects result in webeossite CRs in the same namespace, so the webeeossite CR name
		// must be unique across all user projects - this works because namespaces cannot have `.` in their name
		Name: upd.Namespace + "." + upd.Name,
	}
}

// applicationRegistrationNamespacedName function that given an upd will return the NamespacedName of an ApplicationRegistration which
// allow us to only having to change this function if we want to change the way we call ApplicationRegistrations
func applicationRegistrationNamespacedName(upd webeosv1alpha1.UserProvidedDirectory) types.NamespacedName {
	return types.NamespacedName{
		Namespace: upd.Namespace,
		// ApplicationRegistration is expected to have the same name as its parent namespace
		Name: upd.Namespace,
	}
}

// getOidcReturnUri function that given an upd will return the OidcReturnURI string
// allow us to only having to change this function if we want to change it
func getOidcReturnUri(upd webeosv1alpha1.UserProvidedDirectory) string {
	// site_url + OidcReturnPath (HELM-configured value obtained from environment)
	uri := url.URL{
		Scheme: "https",
		Host:   upd.Spec.SiteUrl,
		Path:   *oidcReturnPath,
	}
	return uri.String()
}

// getOidcSecret retrieves the OIDC secret for the given AppReg object.
func (r *UserProvidedDirectoryReconciler) getOidcSecret(ctx context.Context, upd *webeosv1alpha1.UserProvidedDirectory, appReg *webservicesv1alpha1.ApplicationRegistration) (*corev1.Secret, error) {
	// Check if the ClientCredentialsSecret is set
	if appReg.Status.ClientCredentialsSecret == "" {
		err := generalerrors.New("ClientCredentialsSecret is not set in ApplicationRegistration status")
		r.Log.Error(err, "The ApplicationRegistration resource is missing the ClientCredentialsSecret in its status")
		return nil, err
	}

	// Retrieve OIDC secret needed to provision a WebeosSite
	oidcSecret := &corev1.Secret{}
	oidcSecretNamespaceName := types.NamespacedName{
		Namespace: upd.Namespace,
		Name:      appReg.Status.ClientCredentialsSecret,
	}
	if err := r.Get(ctx, oidcSecretNamespaceName, oidcSecret); err != nil {
		r.Log.Error(err, "Failed to fetch the OIDC client secret generated by the ApplicationRegistration CR in the UserProvidedDirectory namespace")
		return nil, err
	}

	// Validate that the secret contains the required data
	requiredFields := []string{"clientID", "clientSecret", "issuerURL"}
	for _, field := range requiredFields {
		if value, exists := oidcSecret.Data[field]; !exists || len(value) == 0 {
			return nil, fmt.Errorf("secret '%s' in namespace '%s' is missing required field or has empty value: %s", appReg.Status.ClientCredentialsSecret, oidcSecretNamespaceName, field)
		}
	}

	return oidcSecret, nil
}

// getFQDN function that given an upd will return the FQDN string
// allow us to only having to change this function if we want to change it
func getFQDN(upd webeosv1alpha1.UserProvidedDirectory) string {
	return upd.Spec.SiteUrl
}

func projectBlocked(namespace v1.Namespace) bool {
	value, found := namespace.ObjectMeta.Labels[projectBlockedLabel]
	if found && value == "true" {
		return true
	}
	return false
}
