package controllers

import (
	"path/filepath"

	"github.com/go-logr/logr"

	webeosSitev1alpha1 "gitlab.cern.ch/webservices/webeos/webeos-site-operator/api/v1alpha1"

	"net/url"
)

// OidcCryptoPassphrase contains the cryptopassphrase for OIDC
var OidcCryptoPassphrase string

// OIDCCachePath contains the Path to the OIDC Cache folder eg."/var/cache/httpd/mod_auth_openidc/cache"
var OIDCCachePath string

// createSiteDefinitionFromCR will validate and create a structure based on the CRs information
func createSiteDefinitionFromCR(log logr.Logger, webeossite webeosSitev1alpha1.WebeosSiteSpec) (SiteDefinition, []error) {
	err := webeosSitev1alpha1.ValidateSiteDefinitionFromCR(&webeossite)
	if len(err) > 0 {
		return SiteDefinition{}, err
	}

	var CgiPaths []string
	for _, v := range webeossite.Configuration {
		if v.CgiEnabled {
			// Check if v.Path is a path
			absoluteCgiPath := filepath.Join(webeossite.SitePath, v.Path)
			CgiPaths = append(CgiPaths, absoluteCgiPath)
		}
	}

	redirectURL, urlError := url.Parse(webeossite.Oidc.ReturnURI)
	if urlError != nil {
		return SiteDefinition{}, []error{urlError}
	}

	var legacyBaseURL string
	var useLegacyBaseURL bool
	if webeossite.LegacyBaseURL == nil {
		legacyBaseURL = ""
		useLegacyBaseURL = false
	} else {
		legacyBaseURL = webeossite.LegacyBaseURL.Path
		useLegacyBaseURL = webeossite.LegacyBaseURL.PreserveLegacyUrls
	}

	return SiteDefinition{
		ClientID:                         webeossite.Oidc.ClientID,
		ClientSecret:                     webeossite.Oidc.ClientSecret,
		CryptoPassphrase:                 OidcCryptoPassphrase,
		RedirectURI:                      webeossite.Oidc.ReturnURI,
		IssuerURL:                        webeossite.Oidc.IssuerURL,
		RedirectLocation:                 redirectURL.Path,
		FQDN:                             webeossite.HostName,
		DocumentRoot:                     webeossite.SitePath,
		AllowAnonAccessByDefault:         webeossite.AllowAnonAccessByDefault,
		Archived:                         webeossite.Archived,
		LegacyBaseURL:                    legacyBaseURL,
		UseLegacyBaseURL:                 useLegacyBaseURL,
		CgiPaths:                         CgiPaths,
		UseDistributedConfigurationFiles: webeossite.UseDistributedConfigurationFiles,
		ClusterNetworks:                  NetworkList,
		AllowedWebCrawlersIfNoRobotsTxt:  string(webeossite.AllowedWebCrawlersIfNoRobotsTxt),
		OIDCCachePath:                    OIDCCachePath,
	}, nil
}
