package controllers

import (
	"context"
	"flag"
	"fmt"
	"strings"

	"gitlab.cern.ch/webservices/webeos/webeos-site-operator/utils"

	"github.com/go-logr/logr"

	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	routev1 "github.com/openshift/api/route/v1"
	webeosv1alpha1 "gitlab.cern.ch/webservices/webeos/webeos-site-operator/api/v1alpha1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/intstr"
)

const (
	webeosSiteFinalizer = "finalizer.controller-webeossite.webservices.cern.ch"
	// Label key name for route creation
	routerShardLabel      = "webeos.cern.ch/router-shard"
	webeosDeploymentLabel = "webeos.cern.ch/webeos-deployment"
)

var (
	// Register values for command-line flag parsing, by default we add a value
	// In order to interact with this, it is only required to add the pertinent flag
	// when starting the process (e.g. -targetPort another-targetPort)
	targetPortPtr = flag.String("targetPort", "web", "Target port for route creation")

	// List of all router shards available in the cluster - passed as a flag to the operator
	shards strFlagList
)

type strFlagList []string

func (i *strFlagList) String() string {
	return strings.Join(*i, ",")
}

func (i *strFlagList) Set(value string) error {
	*i = append(*i, value)
	return nil
}

// WebeosSiteReconciler reconciles a WebeosSite object
type WebeosSiteReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=webeos.webservices.cern.ch,resources=webeossites,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=webeos.webservices.cern.ch,resources=webeossites/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=route.openshift.io,resources=routes,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=route.openshift.io,resources=routes/custom-host,verbs=get;update;patch

func (r *WebeosSiteReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = r.Log.WithValues("webeossite", req.NamespacedName)

	result := ctrl.Result{}

	r.Log.Info("Reconciling request")

	// Fetch the WebeosSite instance
	instance := &webeosv1alpha1.WebeosSite{}

	err := r.Get(ctx, req.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup
			// logic we use finalizers. Return and don't requeue
			r.Log.Info(fmt.Sprintf("WebeosSite not found; return and don't requeue: %v", req))
			return result, nil
		}
		return result, err
	}

	// Check if the CR was marked to be deleted
	if instance.GetDeletionTimestamp() != nil {
		if err := r.removeFinalizer(instance); err != nil {
			return result, err
		}
		return result, nil
	}

	// Check if finalizer is set, if not, set it and update CR status
	if !utils.Contains(instance.GetFinalizers(), webeosSiteFinalizer) {
		if err := r.addFinalizer(instance); err != nil {
			return result, err
		}
		return result, nil
	}

	// Initialize the status of the CR
	if &instance.Status.ProvisioningStatus == nil {
		r.Log.Info("Initializing status webeos deployment")

		instance.Status = webeosv1alpha1.WebeosSiteStatus{
			ProvisioningStatus: webeosv1alpha1.Creating,
			ErrorReason:        webeosv1alpha1.NoneRS,
			ErrorMessage:       "",
		}
		if err := r.updateCRStatus(instance); err != nil {
			r.Log.Error(err, "Failed to initialize CR with a status %v")
			return result, err
		}
		return result, nil
	}

	// Check if the CR has a correct valid path, if not it will stop the reconcile loop and set a permanent error
	errVal := webeosv1alpha1.ValidateSiteDefinitionFromCR(&instance.Spec)
	if len(errVal) > 0 {
		r.Log.Error(fmt.Errorf(""), "Invalid CR definition, hence can't create site name; return and don't requeue")

		errorMessage := fmt.Sprintf("CR definition has the following errors: %q", errVal)

		instance.Status = webeosv1alpha1.WebeosSiteStatus{
			ProvisioningStatus: webeosv1alpha1.ProvisioningFailed,
			ErrorReason:        webeosv1alpha1.InvalidCRdefinition,
			ErrorMessage:       errorMessage,
		}
		if err := r.updateCRStatus(instance); err != nil {
			return result, err
		}
		return result, nil
	}

	if instance.Spec.ServerDetails == nil {
		instance.Spec.ServerDetails = &webeosv1alpha1.ServerDetails{}
	}

	// assign new sites to a router shard and webeos deployment
	if instance.Spec.ServerDetails.AssignedRouterShard == "" || instance.Spec.ServerDetails.AssignedDeploymentShard == "" {
		if err := assignRouterShard(instance, r.Log); err != nil {
			return result, err
		}

		if err = r.assignWebEOSDeployment(instance); err != nil {
			return result, err
		}

		if err := r.updateCR(instance); err != nil {
			r.Log.Error(err, "Failed to update WebEosSite.Spec.ServerDetails RouterShard or DeploymentShard %v")
			return result, err
		}

		return result, nil
	}

	// Set labels to webeos deployment if none or mismatch
	if !r.hasCorrectLabels(instance) {
		r.setLabels(instance)
		if err := r.updateCR(instance); err != nil {
			return result, err
		}
		return result, nil
	}

	// Ensure route exists with the expected content.
	route := &routev1.Route{
		ObjectMeta: metav1.ObjectMeta{
			Name:      instance.Name,
			Namespace: instance.Namespace,
		}}
	if _, err := controllerutil.CreateOrUpdate(ctx, r.Client, route, func() error {
		return r.routeForWebeosSite(route, instance)
	}); err != nil {
		return ctrl.Result{}, err
	}

	if instance.Status.ProvisioningStatus != webeosv1alpha1.Created {
		instance.Status = webeosv1alpha1.WebeosSiteStatus{
			ProvisioningStatus: webeosv1alpha1.Created,
			ErrorReason:        webeosv1alpha1.NoneRS,
			ErrorMessage:       "",
		}
		if err := r.updateCRStatus(instance); err != nil {
			return result, err
		}
		return result, nil
	}

	r.Log.Info("WebeosSite reconciled name")
	return result, nil
}

func init() {
	flag.Var(&shards, "assignable-router-shard", "List of available router shards")
}

func (r *WebeosSiteReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&webeosv1alpha1.WebeosSite{}).
		Owns(&routev1.Route{}).
		Complete(r)
}

func (r *WebeosSiteReconciler) hasCorrectLabels(instance *webeosv1alpha1.WebeosSite) bool {
	if len(instance.GetLabels()) == 0 || instance.Labels[routerShardLabel] != instance.Spec.ServerDetails.AssignedRouterShard ||
		instance.Labels[webeosDeploymentLabel] != instance.Spec.ServerDetails.AssignedDeploymentShard {
		return false
	}
	return true
}

func (r *WebeosSiteReconciler) setLabels(instance *webeosv1alpha1.WebeosSite) {
	labels := instance.GetLabels()

	if labels == nil {
		labels = make(map[string]string)
	}

	labels[routerShardLabel] = instance.Spec.ServerDetails.AssignedRouterShard
	labels[webeosDeploymentLabel] = instance.Spec.ServerDetails.AssignedDeploymentShard

	instance.SetLabels(labels)
}

// routeForWebeosSite returns a route object
func (r *WebeosSiteReconciler) routeForWebeosSite(route *routev1.Route, instance *webeosv1alpha1.WebeosSite) error {
	if route.Labels == nil {
		route.Labels = map[string]string{}
	}
	route.Labels[routerShardLabel] = instance.Spec.ServerDetails.AssignedRouterShard
	route.Labels[webeosDeploymentLabel] = instance.Spec.ServerDetails.AssignedDeploymentShard
	if route.Annotations == nil {
		route.Annotations = map[string]string{}
	}
	/*
		This annotation will make the Router replace the X-Forwarded-For header with the real IP of the requester.
		The objective is to avoid spoofing client IP address when accessing intranet-only websites
		https://gitlab.cern.ch/webservices/webframeworks-planning/-/issues/93
	*/
	route.Annotations["haproxy.router.openshift.io/set-forwarded-headers"] = "replace"
	route.Annotations["haproxy.router.openshift.io/ip_whitelist"] = instance.Spec.ServerDetails.RouteIpWhitelist

	route.Spec = routev1.RouteSpec{
		TLS: &routev1.TLSConfig{
			InsecureEdgeTerminationPolicy: "Redirect",
			Termination:                   "edge",
		},
		To: routev1.RouteTargetReference{
			Kind: "Service",
			Name: instance.Spec.ServerDetails.AssignedDeploymentShard,
		},
		Host: instance.Spec.HostName,
		Port: &routev1.RoutePort{
			TargetPort: intstr.FromString(*targetPortPtr),
		},
	}
	return controllerutil.SetControllerReference(instance, route, r.Scheme)
}

// RemoveFinalizer will execute the necessary actions to clean up resources created by
// the existance of the CR and finally will remove the finalizer from WebeosSite CR so
// that it can be deleted
func (r *WebeosSiteReconciler) removeFinalizer(instance *webeosv1alpha1.WebeosSite) error {
	if utils.Contains(instance.GetFinalizers(), webeosSiteFinalizer) {
		// Remove webeosSiteFinalizer. Once all finalizers have been
		// removed, the object will be deleted.
		instance.SetFinalizers(utils.Remove(instance.GetFinalizers(), webeosSiteFinalizer))
		err := r.Update(context.TODO(), instance)
		if err != nil {
			r.Log.Error(err, "Failed to update a CR when removing the finalizer with err: %v")
			return err
		}
	}
	return nil
}

// AddFinalizer will add a finalizer to the WebeosSite CR so that
// we can delete it appropriately
func (r *WebeosSiteReconciler) addFinalizer(instance *webeosv1alpha1.WebeosSite) error {
	r.Log.Info("Adding Finalizer to the WebeosSite")
	instance.SetFinalizers(append(instance.GetFinalizers(), webeosSiteFinalizer))
	if err := r.updateCR(instance); err != nil {
		r.Log.Error(err, "Failed to update a CR with a finalizer %v")
		return err
	}
	return nil
}

func (r *WebeosSiteReconciler) updateCR(instance *webeosv1alpha1.WebeosSite) error {
	if err := r.Update(context.TODO(), instance); err != nil {
		r.Log.Error(err, "Failed to update WebeosSite with err: %v")
		return err
	}
	return nil
}

func (r *WebeosSiteReconciler) updateCRStatus(instance *webeosv1alpha1.WebeosSite) error {
	if err := r.Status().Update(context.TODO(), instance); err != nil {
		r.Log.Error(err, "Failed to update WebeosSite status with err: %v")
		return err
	}
	return nil
}
