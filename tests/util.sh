# Sometimes given that operators take a bit more to start reconciling properly or simply take a bit more to properly reconcile an object
# this function will try to make the test assertion 100 times and if at the end we do not obtain the expected behaviour that's because something is wrong
function try-to-execute () {
    for run in {1..100}; do
        sleep 1
        RESULT=$(eval $1 2>&1)
        if test "$RESULT" == "$2" ; then
            return 0
        fi
    done
    echo "Command "$1" failed to run more than 100 times"  >&2
    exit 1
}