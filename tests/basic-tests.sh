set -e

source tests/util.sh

echo -n Creating WebEOS deployments
oc create -f tests/resources/webeos-site-services.yml > /dev/null
echo " [OK]"

echo -n Creating a WebEOS site route and verifying all the necessary things
oc create -f tests/resources/webeos-site.yaml > /dev/null
echo " [OK]"

echo -n Check if the route was created with the name of the CR created
try-to-execute "oc get route mysite -o json | jq -r '.metadata.name'" "mysite"
echo " [OK]"

echo -n Check if the route was created with the proper description
try-to-execute "oc get route mysite -o json | jq -r '.metadata.labels.\"webeos.cern.ch/router-shard\" | test(\"^(router-shard-1|router-shard-2|router-shard-3)\$\")' " "true"
echo " [OK]"

echo -n Check if the route was created with the annotation for Apache "haproxy.router.openshift.io/set-forwarded-headers"
try-to-execute "oc get route mysite -o json | jq -r '.metadata.annotations.\"haproxy.router.openshift.io/set-forwarded-headers\"'" "replace"
echo " [OK]"

echo -n Check if targetPort exists
try-to-execute "oc get route mysite -o json | jq -r '.spec.port.targetPort'" "web"
echo " [OK]"

echo -n Check if we can add a random label in the webeossite and this does not dissapear [*1]
oc label webeossite mysite myRandomLabel="label-has-to-stay" > /dev/null
try-to-execute "oc get webeossite/mysite -o json | jq -r '.metadata.labels.myRandomLabel'" "label-has-to-stay"
echo " [OK]"

echo -n Check if we can add a random label in the route and this does not dissapear [*2]
oc label route mysite myRandomLabel="label-has-to-stay" > /dev/null
try-to-execute "oc get route/mysite -o json | jq -r '.metadata.labels.myRandomLabel'" "label-has-to-stay"
echo " [OK]"

echo -n Check if we can add a random annotation in the route and this does not dissapear
oc annotate route mysite myRandomAnnotation="annotation-has-to-stay" > /dev/null
try-to-execute "oc get route/mysite -o json | jq -r '.metadata.annotations.myRandomAnnotation'" "annotation-has-to-stay"
echo " [OK]"

echo -n Check if we can update the Route HostName successfully after updating in CR
oc patch webeossite mysite -p '{"spec":{"hostName":"my.hostname2"}}' --type='merge' > /dev/null
try-to-execute "oc get route mysite -o json | jq -r '.spec.host'" "my.hostname2"
echo " [OK]"

echo -n Check if we enforce webeossite policy and we can NOT modify the targetPort after updating the route
oc patch route mysite -p '{"spec":{"port":{"targetPort": "web-wrong"}}}' --type='merge' > /dev/null
try-to-execute "oc get route mysite -o json | jq -r '.spec.port.targetPort'" "web"
echo " [OK]"

echo -n Check if we enforce lable changes for serverDetails.assignedDeploymentShard
oc patch webeossite mysite -p '{"spec":{"serverDetails":{"assignedDeploymentShard": "webeos-test"}}}' --type='merge' > /dev/null
try-to-execute "oc get webeossite mysite -o json | jq -r '.metadata.labels.\"webeos.cern.ch/webeos-deployment\"'" "webeos-test"
echo " [OK]"

echo -n Check if we enforce label changes for serverDetails.assignedRouterShard
oc patch webeossite mysite -p '{"spec":{"serverDetails":{"assignedRouterShard": "router-shard-5"}}}' --type='merge' > /dev/null
try-to-execute "oc get webeossite mysite -o json | jq -r '.metadata.labels.\"webeos.cern.ch/router-shard\"'" "router-shard-5"
echo " [OK]"

echo -n Check if we enforce webeossite policy and we can NOT modify the labels in the route after updating the route
oc patch route mysite -p '{"spec":{"metadata":{"labels": {"webeos.cern.ch/webeos-deployment": "wrong-label"}}}}' --type='merge' > /dev/null
try-to-execute "oc get route/mysite -o json | jq -r '.metadata.labels.\"webeos.cern.ch/webeos-deployment\"'" "webeos-test"
echo " [OK]"

echo -n Check if the random label earlier added in the webeossite did not dissapear after reconcile [*1]
try-to-execute "oc get webeossite/mysite -o json | jq -r '.metadata.labels.myRandomLabel'" "label-has-to-stay"
echo " [OK]"

echo -n Check if the random label earlier added in the route did not dissapear after reconcile [*2]
try-to-execute "oc get route/mysite -o json | jq -r '.metadata.labels.myRandomLabel'" "label-has-to-stay"
echo " [OK]"

echo -n Check if the random annotation earlier added in the route did not dissapear after reconcile
try-to-execute "oc get route/mysite -o json | jq -r '.metadata.annotations.myRandomAnnotation'" "annotation-has-to-stay"
echo " [OK]"

echo -n Check if the label automatically earlier added from the webeossite for serverDetails.assignedRouterShard is in the route
try-to-execute "oc get route/mysite -o json | jq -r '.metadata.labels.\"webeos.cern.ch/webeos-deployment\"'" "webeos-test"
echo " [OK]"

echo -n Check if the useDistributedConfigurationFiles spec boolean value is added by default to false
try-to-execute "oc get webeossite/mysite -o json | jq '.spec.useDistributedConfigurationFiles'" "false"
echo " [OK]"

echo -n Deleting webeossite custom resource
oc delete -f tests/resources/webeos-site.yaml > /dev/null
echo " [OK]"

echo -n Deleting WebEOS deployments
oc delete -f tests/resources/webeos-site-services.yml > /dev/null
echo " [OK]"

# We need to switch to set +e due to the error output of the commands we are executing
set +e

echo -n Check if the CR was deleted
try-to-execute "oc get webeossite mysite -o json" "Error from server (NotFound): webeossites.webeos.webservices.cern.ch \"mysite\" not found"
echo " [OK]"

echo -n Check if the CR deleted the route
try-to-execute "oc get route mysite -o json | jq -r '.metadata.name'" "Error from server (NotFound): routes.route.openshift.io \"mysite\" not found"
echo " [OK]"

