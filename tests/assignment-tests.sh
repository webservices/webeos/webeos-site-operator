set -e

source tests/util.sh

echo -n Creating WebEOS deployments
oc create -f tests/resources/webeos-site-services.yml > /dev/null
echo " [OK]"

echo -n Creating a WebEOS site CR
oc create -f tests/resources/webeos-site-assigned.yml > /dev/null
echo " [OK]"

echo -n Check assigned router shard
try-to-execute "oc get webeossite assigning-example -o json | jq '.spec.serverDetails.assignedRouterShard | test(\"^(router-shard-1|router-shard-2|router-shard-3)\$\")'" "true"
try-to-execute "oc get webeossite assigning-example-2 -o json | jq '.spec.serverDetails.assignedRouterShard | test(\"^(router-shard-1|router-shard-2|router-shard-3)\$\")'" "true"
echo " [OK]"

echo -n Check assigned deployment shard
try-to-execute "oc get webeossite assigning-example -o json | jq '.spec.serverDetails.assignedDeploymentShard | test(\"^(webeos-project-1|webeos-project-2|webeos-default)\$\")'" "true"
try-to-execute "oc get webeossite assigning-example-2 -o json | jq -r '.spec.serverDetails.assignedDeploymentShard'" "webeos-default"
echo " [OK]"

echo -n Deleted the WebEOS CR
oc delete -f tests/resources/webeos-site-services.yml > /dev/null
oc delete -f tests/resources/webeos-site-assigned.yml > /dev/null
echo " [OK]"
