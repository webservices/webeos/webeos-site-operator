/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// OidcReturnURISpec defines the desired state of OidcReturnURI
type OidcReturnURISpec struct {
	// RedirectURI is an OIDC redirect URI for the ApplicationRegistration in the same namespace
	RedirectURI string `json:"redirectURI,omitempty" valid:"url"`
}

// OidcReturnURIStatus defines the observed state of OidcReturnURI
type OidcReturnURIStatus struct {
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// OidcReturnURI is the Schema for the oidcreturnuris API
type OidcReturnURI struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   OidcReturnURISpec   `json:"spec,omitempty"`
	Status OidcReturnURIStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// OidcReturnURIList contains a list of OidcReturnURI
type OidcReturnURIList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []OidcReturnURI `json:"items"`
}

func init() {
	SchemeBuilder.Register(&OidcReturnURI{}, &OidcReturnURIList{})
}

// SameSet asserts whether 2 []string contain the same elements, disregarding order/multiplicity (set equality)
func SameSet(a []string, b []string) bool {
	contains := func(a []string, b []string) bool {
		m := make(map[string]bool, len(a))
		for _, i := range a {
			m[i] = true
		}
		same := true
		for _, i := range b {
			if !m[i] {
				same = false
				break
			}
		}
		return same
	}
	return contains(a, b) && contains(b, a)
}
