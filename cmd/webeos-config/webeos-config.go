package main

import (
	"flag"
	"fmt"
	"os"

	config "github.com/openshift/api/config/v1"
	webeosv1alpha1 "gitlab.cern.ch/webservices/webeos/webeos-site-operator/api/v1alpha1"
	"gitlab.cern.ch/webservices/webeos/webeos-site-operator/controllers"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"

	"github.com/spf13/pflag"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	// +kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))
	utilruntime.Must(webeosv1alpha1.AddToScheme(scheme))
	utilruntime.Must(config.AddToScheme(scheme))
	// +kubebuilder:scaffold:scheme
}

func main() {
	// Add flags registered by imported packages (e.g. glog and
	// controller-runtime)
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)

	metricsAddr := pflag.String("metrics-addr", ":8383", "The address the metric endpoint binds to.")
	enableLeaderElection := pflag.Bool("enable-leader-election", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")

	// In the case of an empty labelSelector, it will work with every CR
	LabelSelector := pflag.String("label-selector", "", "A label filter to select only the webeos sites assigned to the deployment the operator is running in")

	// Create Command
	apachePath := pflag.String("apache-path", "", "ApachePath, variable containing the path of the Apache Server folder")
	vhostTemplatePath := pflag.String("vhost-template-path", "/conf/vhost_config.template", "VhostTemplatePath, variable containing the path of the template file for the Vhost creation")
	oidcCryptoPassphrase := pflag.String("oidc-cryptopassphrase", "", "OidcCryptoPassphrase, variable containing the cryptopassphrase for OIDC configuration")
	readinessProbePath := pflag.String("readiness-probe-path", "", "readinessProbePath, variable containing the path to the file to touch when every CR has been processed once")
	oidcCachePath := pflag.String("oidc-cache-path", "/var/cache/httpd/mod_auth_openidc/cache", "OIDCCachePath, variable containing the path of the OIDC Cache folder")

	pflag.Parse()

	ctrl.SetLogger(zap.New(zap.UseDevMode(true)))

	var err error
	controllers.LabelSelector, err = metav1.ParseToLabelSelector(*LabelSelector)

	if err != nil {
		setupLog.Error(err, "Could not parse the label-selector flag. Make sure it's a valid Kubernetes label selector.")
		os.Exit(1)
	}

	if *apachePath == "" {
		setupLog.Error(nil, "Failed, ApachePath argument not provided")
		os.Exit(1)
	}
	if *vhostTemplatePath == "" {
		setupLog.Error(nil, "Failed, VhostTemplatePath argument not provided")
		os.Exit(1)
	}
	if *oidcCryptoPassphrase == "" {
		setupLog.Error(nil, "Failed, OidcCryptoPassphrase argument not provided")
		os.Exit(1)
	}

	controllers.ApachePath = *apachePath
	controllers.OidcCryptoPassphrase = *oidcCryptoPassphrase
	controllers.LoadTemplate(*vhostTemplatePath)
	controllers.ReadinessProbePath = *readinessProbePath
	controllers.OIDCCachePath = *oidcCachePath

	watchNamespace, err := getWatchNamespace()
	if err != nil {
		setupLog.Error(err, "Failed to get watch namespace")
		os.Exit(1)
	}

	cfg := ctrl.GetConfigOrDie()

	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	mgr, err := ctrl.NewManager(cfg, ctrl.Options{
		Scheme:             scheme,
		MetricsBindAddress: *metricsAddr,
		Port:               9443,
		LeaderElection:     *enableLeaderElection,
		LeaderElectionID:   "a30a83e4.webservices.cern.ch",
		Namespace:          watchNamespace,
	})

	// PrepareRedinessProbe is a function used on the main to make a first iteration through all the CRs
	err = controllers.PrepareReadinessProbe(watchNamespace)
	if err != nil {
		setupLog.Error(err, "Failed to prepare Readiness probe")
		os.Exit(1)
	}

	// Create dedicated Client to get Pod Network
	// This is so we can properly configure Apache's mod_remoteip with the possible IP addresses of Openshift ingress/router pods
	clt, err := client.New(cfg, client.Options{Scheme: scheme})
	if err != nil {
		setupLog.Error(err, "Failed to create dedicated client")
		os.Exit(1)
	}

	err = controllers.GetPodNetwork(clt)
	if err != nil {
		setupLog.Error(err, "Failed to retrieve Pod Network")
		os.Exit(1)
	}

	if err = (&controllers.ReconcileWebeosSite{
		Client: mgr.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("WebeosConfig"),
		Scheme: mgr.GetScheme(),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "WebeosConfig")
		os.Exit(1)
	}
	// +kubebuilder:scaffold:builder

	setupLog.Info("starting webeos-config manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}

func getWatchNamespace() (string, error) {
	// WatchNamespaceEnvVar is the constant for env variable WATCH_NAMESPACE
	// which specifies the Namespace to watch.
	// An empty value means the operator is running with cluster scope.
	var watchNamespaceEnvVar = "WATCH_NAMESPACE"

	ns, found := os.LookupEnv(watchNamespaceEnvVar)
	if !found {
		return "", fmt.Errorf("%s must be set", watchNamespaceEnvVar)
	}
	return ns, nil
}
