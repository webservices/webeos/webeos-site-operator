package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WebeosSiteSpec defines the desired state of WebeosSite
type WebeosSiteSpec struct {

	// Site description (e.g. "Hello mysite name")
	// +kubebuilder:validation:MaxLength=255
	// +kubebuilder:validation:MinLength=1
	SiteDescription string `json:"siteDescription"`

	// Site type, possible values: centrallyManagedEosFolder, ownerProvidedEosFolder
	// +kubebuilder:validation:Enum=centrallyManagedEosFolder;ownerProvidedEosFolder
	Type string `json:"type"`

	// WebEOS site path
	// +kubebuilder:validation:MaxLength=255
	// +kubebuilder:validation:MinLength=1
	SitePath string `json:"sitePath,omitempty"`

	// LegacyBaseURL contains all values regarding legacy URLs. It only applies to legacy sites migrated from the old
	// webeos infrastructure that used base URLs like https://mysite.web.cern.ch/mysite and is not used for new sites.
	LegacyBaseURL *LegacyBaseURL `json:"legacyBaseURL,omitempty"`

	// Site configuration of EOS paths
	Configuration []WebeosSiteConfigurationPath `json:"configuration,omitempty"`

	// Server details of the site, which routerShard and deploymentShrad are using
	ServerDetails *ServerDetails `json:"serverDetails,omitempty"`

	// HostName to be used while creating the Route
	// +kubebuilder:validation:MaxLength=255
	// +kubebuilder:validation:MinLength=1
	HostName string `json:"hostName,omitempty"`

	// Site owner to be used while creating the Route
	// +kubebuilder:validation:MaxLength=255
	// +kubebuilder:validation:MinLength=1
	Owner string `json:"owner,omitempty"`

	// This field if True ensures the site is accessible anonymously by default (compatibility for legacy sites)
	// or not (all new sites should not be accessible anonymously be default as per Security Team recommendations)
	// +kubebuilder:validation:Enum=true;false
	AllowAnonAccessByDefault bool `json:"allowAnonAccessByDefault,omitempty"`

	// This field if True makes the Site Archived
	// +kubebuilder:validation:XPreserveUnknownFields
	// +kubebuilder:validation:Enum=true;false
	// +kubebuilder:default:=false
	Archived bool `json:"archived,omitempty"`

	// OIDC: ClientID and ClientSecret parameters for the OIDC-based SSO
	Oidc *Oidc `json:"oidc,omitempty"`

	// Allows to override web server configuration using .htaccess files. Site access will be slower if enabled. If disabled, .htaccess files are ignored.
	// `true` results in `AllowOverride All`; `false` results in `AllowOverride None`.
	// +kubebuilder:validation:Enum=true;false
	UseDistributedConfigurationFiles bool `json:"useDistributedConfigurationFiles"`

	// Define whether crawling by public/CERN crawlers should be enabled.
	// In case there is a robots.txt file in the EOS path, this option will not be respected and your custom robots.txt will be used.
	AllowedWebCrawlersIfNoRobotsTxt CrawlingModeConfiguration `json:"allowedWebCrawlersIfNoRobotsTxt,omitempty"`

	// The serverVersion defines from which webeos deployment the site will be served.
	// +kubebuilder:validation:Enum:=el9
	// +kubebuilder:default:=el9
	ServerVersion ServerVersionType `json:"serverVersion,omitempty"`
}

// WebeosSiteStatus defines the observed state of WebeosSite
type WebeosSiteStatus struct {
	// Return code of the last operation
	ProvisioningStatus ProvisioningStatus `json:"provisioningStatus,omitempty"`

	// Error reason in case of error
	ErrorReason ErrorReason `json:"errorReason,omitempty"`

	// Error message in case of error
	ErrorMessage string `json:"errorMessage,omitempty"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=ws
// +kubebuilder:printcolumn:name="Hostname",type=string,JSONPath=`.spec.hostName`
// +kubebuilder:printcolumn:name="EOS Path",type=string,JSONPath=`.spec.sitePath`
// +kubebuilder:printcolumn:name="Deployment",type=string,JSONPath=`.spec.serverDetails.assignedDeploymentShard`
// +kubebuilder:printcolumn:name="Router",type=string,JSONPath=`.spec.serverDetails.assignedRouterShard`
// WebeosSite is the Schema for the webeossites API
type WebeosSite struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   WebeosSiteSpec   `json:"spec,omitempty"`
	Status WebeosSiteStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// WebeosSiteList contains a list of WebeosSite
type WebeosSiteList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []WebeosSite `json:"items"`
}

// LegacyBaseURL contains all values regarding legacy URLs. It only applies to legacy sites migrated from the old
// webeos infrastructure that used base URLs like https://mysite.web.cern.ch/mysite and is not used for new sites.
type LegacyBaseURL struct {
	// This field will contain the value that will be added to the site URL according to
	// PreserveLegacyUrls
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:MinLength=1
	Path string `json:"path,omitempty"`

	// This field if True then the operator will add path to the site URL
	// then redirect mysite.web.cern.ch -> mysite.web.cern.ch/mysite (keep old URL scheme)
	// if false, then redirect mysite.web.cern.ch/mysite -> mysite.web.cern.ch (use new URL scheme)
	// +kubebuilder:validation:Enum=true;false
	PreserveLegacyUrls bool `json:"preserveLegacyUrls,omitempty"`
}

type ServerDetails struct {
	// We must set MaxLength to 63 characters as this value is stored in Kubernetes labels and there is limited
	// length as mentioned in
	// https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set
	// +kubebuilder:validation:MaxLength=63
	AssignedRouterShard string `json:"assignedRouterShard,omitempty"`

	// By default, a deployment name can contain up to 253 characters as explained in
	// https://kubernetes.io/docs/concepts/overview/working-with-objects/names/#dns-subdomain-names
	// But currently we must set MaxLength to 63 characters as this value is stored in Kubernetes labels and there
	// is limited length as mentioned in
	// https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/#syntax-and-character-set
	// +kubebuilder:validation:MaxLength=63
	AssignedDeploymentShard string `json:"assignedDeploymentShard,omitempty"`

	// Allows to set the ip_whitelist annotation on the route
	// +optional
	// +kubebuilder:default:=""
	RouteIpWhitelist string `json:"routeIpWhitelist"`
}

type Oidc struct {
	// +kubebuilder:validation:MaxLength=255
	ClientID string `json:"clientID"`
	// +kubebuilder:validation:MaxLength=50
	ClientSecret string `json:"clientSecret"`
	IssuerURL    string `json:"issuerURL"`
	ReturnURI    string `json:"returnURI"`
}

// WebeosSiteConfigurationPath contains a configuration path
type WebeosSiteConfigurationPath struct {
	// +kubebuilder:validation:MaxLength=255
	Path string `json:"path,omitempty"`
	// +kubebuilder:validation:Enum=true;false
	CgiEnabled bool `json:"cgiEnabled,omitempty"`
}

// ProvisioningStatus contain the status if the site was provisioned
type ProvisioningStatus string

const (
	Creating           ProvisioningStatus = "creating"
	Created            ProvisioningStatus = "created"
	ProvisioningFailed ProvisioningStatus = "provisioningFailed"
)

// ErrorReason of the last operation
type ErrorReason string

const (
	NoneRS              ErrorReason = "none"
	InvalidName         ErrorReason = "invalidName"
	InvalidCRdefinition ErrorReason = "invalidCRdefinition"
	RouteCreationFailed ErrorReason = "routeCreationFailed"
)

func init() {
	SchemeBuilder.Register(&WebeosSite{}, &WebeosSiteList{})
}
