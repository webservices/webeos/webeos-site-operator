package v1alpha1

import (
	"testing"
)

func TestValidateSitePath(t *testing.T) {
	testCases := []struct {
		SitePath   string
		ShouldFail bool
	}{
		{SitePath: "", ShouldFail: true},
		{SitePath: "/", ShouldFail: false},
		{SitePath: "/eos/test", ShouldFail: false},
		{SitePath: "/eos/test/TEST", ShouldFail: false},
		{SitePath: "/eos/test/TEST/", ShouldFail: true},
		{SitePath: "/eos/test/TEST/test-_;", ShouldFail: false},
	}

	for _, testCase := range testCases {
		failed := validateSitePath(testCase.SitePath) != nil
		if failed != testCase.ShouldFail {
			t.Errorf("Failed test case for %s", testCase.SitePath)
		}
	}
}

func TestValidateLegacyBaseURL(t *testing.T) {
	testCases := []struct {
		LegacyBaseURL *LegacyBaseURL
		ShouldFail    bool
	}{
		{LegacyBaseURL: nil, ShouldFail: false},
		{LegacyBaseURL: &LegacyBaseURL{Path: ""}, ShouldFail: true},
		{LegacyBaseURL: &LegacyBaseURL{Path: "/"}, ShouldFail: false},
		{LegacyBaseURL: &LegacyBaseURL{Path: "/test"}, ShouldFail: false},
		{LegacyBaseURL: &LegacyBaseURL{Path: "/test/TEST"}, ShouldFail: false},
		{LegacyBaseURL: &LegacyBaseURL{Path: "/test/TEST_-;"}, ShouldFail: false},
	}

	for _, testCase := range testCases {
		failed := validateLegacyBaseURL(testCase.LegacyBaseURL, "example.com") != nil
		if failed != testCase.ShouldFail {
			t.Errorf("Failed test case for %+v", testCase.LegacyBaseURL)
		}
	}
}

func TestValidateConfiguration(t *testing.T) {
	testCases := []struct {
		Configuration  []WebeosSiteConfigurationPath
		NumberOfErrors int
	}{
		{
			Configuration: []WebeosSiteConfigurationPath{
				{Path: "", CgiEnabled: false},
				{Path: "/", CgiEnabled: false},
				{Path: "/HOME/TEST", CgiEnabled: false},
				{Path: "/HOME/TEST/test_-;", CgiEnabled: false},
				{Path: "/HOME/TEST/", CgiEnabled: false},
			},
			NumberOfErrors: 2,
		},
		{
			Configuration: []WebeosSiteConfigurationPath{
				{Path: "", CgiEnabled: true},
				{Path: "/", CgiEnabled: true},
				{Path: "/HOME/TEST", CgiEnabled: true},
				{Path: "/test/test", CgiEnabled: true},
				{Path: "/test/test/test_-;", CgiEnabled: true},
				{Path: "/HOME/TEST/", CgiEnabled: true},
			},
			NumberOfErrors: 2,
		},
	}

	for _, testCase := range testCases {
		numberOfActualErrors := len(validateConfiguration(testCase.Configuration))
		if numberOfActualErrors != testCase.NumberOfErrors {
			t.Errorf("Failed test case for %+v, expected %d errors but got %d", testCase.Configuration, testCase.NumberOfErrors, numberOfActualErrors)
		}
	}
}

func TestValidateOidc(t *testing.T) {
	testCases := []struct {
		OIDC           *Oidc
		NumberOfErrors int
	}{
		{OIDC: nil, NumberOfErrors: 1},
		{OIDC: &Oidc{ClientID: "", ClientSecret: "", IssuerURL: "", ReturnURI: ""}, NumberOfErrors: 4},
		{OIDC: &Oidc{ClientID: "ClientId", ClientSecret: "", IssuerURL: "IssuerURL", ReturnURI: "ReturnURI"}, NumberOfErrors: 1},
		{OIDC: &Oidc{ClientID: "", ClientSecret: "ClientSecret", IssuerURL: "IssuerURL", ReturnURI: "ReturnURI"}, NumberOfErrors: 1},
		{OIDC: &Oidc{ClientID: "ClientId", ClientSecret: "ClientSecret", IssuerURL: "", ReturnURI: "ReturnURI"}, NumberOfErrors: 1},
		{OIDC: &Oidc{ClientID: "ClientId", ClientSecret: "ClientSecret", IssuerURL: "IssuerURL", ReturnURI: ""}, NumberOfErrors: 1},
		{OIDC: &Oidc{ClientID: "ClientId", ClientSecret: "ClientSecret", IssuerURL: "IssuerURL", ReturnURI: "ReturnURI"}, NumberOfErrors: 0},
	}

	for _, testCase := range testCases {
		numberOfActualErrors := len(validateOidc(testCase.OIDC))
		if numberOfActualErrors != testCase.NumberOfErrors {
			t.Errorf("Failed test case for %+v, expected %d errors but got %d", testCase.OIDC, testCase.NumberOfErrors, numberOfActualErrors)
		}
	}
}
