FROM golang:1.20 AS builder

WORKDIR /workspace
# Copy the vendor folder
COPY go.mod go.mod
COPY go.sum go.sum
COPY vendor/ vendor/

# Copy the go source
COPY cmd/ cmd/
COPY api/ api/
COPY controllers/ controllers/
COPY utils/ utils/

# Build
RUN CGO_ENABLED=0 go build -mod=vendor -o operator cmd/operator/operator.go && \
    CGO_ENABLED=0 go build -mod=vendor -o webeos-config cmd/webeos-config/webeos-config.go

# Use ubi-minimal instead of the default distroless image
# to make sure that we have `test` binary which is used for readiness probes in webeos-config
FROM registry.access.redhat.com/ubi8/ubi-minimal:latest
WORKDIR /
COPY --from=builder /workspace/operator /bin/
COPY --from=builder /workspace/webeos-config /bin/

# Copy vhost template
COPY conf /conf

USER nonroot:nonroot
CMD ["/bin/operator"]
